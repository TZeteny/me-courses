# Párhuzamos eszközök programozása

A tárgy célja, hogy bemutassa az OpenCL szabvány elemeit, példák segítségével annak használatát.

## Technikai tudnivalók

* Fejlesztőkörnyezet: [c_sdk_230222.zip](https://www.uni-miskolc.hu/~matip/_downloads/c_sdk_230222.zip)
* GPU Caps Viewer: [GPU_Caps_Viewer_1.58.0.1.zip](https://www.uni-miskolc.hu/~matip/_downloads/GPU_Caps_Viewer_1.58.0.1.zip)
* Forráskódok, példák: [https://gitlab.com/imre-piller/me-courses/-/tree/master/opencl](https://gitlab.com/imre-piller/me-courses/-/tree/master/opencl)
* A feladatok megoldását mindenkinek Git repository-ba kellene majd feltöltenie.

## Javasolt irodalom

* David Kaeli, Perhaad Mistry, Dana Schaa, Dong Ping Zhang: Heterogeneous Computing with OpenCL 2.0, Morgan Kauffmann, Elsevier, ISBN: 978-0-12-801414-1
* Matthew Scarpino: OpenCL in Action, Manning Publications 2012.
