Irodalmak
=========

Git
---

* https://git-scm.com/book/en/v2
* https://www.oreilly.com/library/view/version-control-with/9781449345037/
* https://www.amazon.com/Git-Teams-User-Centered-Efficient-Workflows/dp/1491911182

Python
------

* https://docs.python.org/3/
* https://realpython.com/
* https://www.oreilly.com/library/view/learning-python-5th/9781449355722/

Falcon
* https://falcon.readthedocs.io/en/stable/


JavaScript
----------

HTML 5 Canvas
* https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API
* https://bucephalus.org/text/CanvasHandbook/CanvasHandbook.html
