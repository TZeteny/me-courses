#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <time.h>

const int N_THREADS = 20;

struct timespec start_time;

/**
 * Calculate the time in seconds from the start of the application.
 */
double get_time()
{
  struct timespec current_time;
  double time;

  clock_gettime(CLOCK_MONOTONIC_RAW, &current_time);
  time = (current_time.tv_sec - start_time.tv_sec);
  time += (double)(current_time.tv_nsec - start_time.tv_nsec) / 1000000000;

  return time;
}

/**
 * Wait random time.
 */
void wait_random_time(void* param)
{
  pid_t tid;
  int wait_time;

  tid = gettid();
  wait_time = *(int*)param;
  printf("[%d] Wait %d seconds ... (%lf)\n", tid, wait_time, get_time());
  sleep(wait_time);
  printf("[%d] Ready! (%lf)\n", tid, get_time());
}

/**
 * Main
 */
int main(int argc, char* argv[])
{
  pthread_t threads[N_THREADS];
  int wait_time;
  int i;

  clock_gettime(CLOCK_MONOTONIC_RAW, &start_time);

  printf(":: Start threads ...\n");
  for (i = 0; i < N_THREADS; ++i) {
    wait_time = rand() % 10 + 1;
    pthread_create(&threads[i], NULL, wait_random_time, &wait_time);
  }

  printf(":: Join threads ...\n");
  for (i = 0; i < N_THREADS; ++i) {
    pthread_join(threads[i], NULL);
  }

  printf(":: Ready.\n");

  return 0;
}
