Párhuzamos algoritmusok
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   gyak_1
   gyak_2
   gyak_3
   gyak_4
   gyak_5
   gyak_6
   gyak_7


Diasorok és egyéb anyagok
-------------------------

* Előadás fóliák: `parh_alg_ea.pdf <https://www.uni-miskolc.hu/~matolaj/parh_alg_ea.pdf>`_
* Gyakorlati fóliák: `parh_alg_gyak.pdf <https://www.uni-miskolc.hu/~matolaj/parh_alg_gyak.pdf>`_
* Fejlesztőkörnyezet MultiPascal-hoz: `parallel_sdk.zip <https://www.uni-miskolc.hu/~matip/parallel/parallel_sdk.zip>`_
* További mintakódok: `parh_alg_sample_codes.zip <https://www.uni-miskolc.hu/~matolaj/parh_alg_sample_codes.zip>`_
* Párhuzamos algoritmusok webes jegyzet: `https://gyires.inf.unideb.hu/KMITT/c08/index.html <https://gyires.inf.unideb.hu/KMITT/c08/index.html>`_
