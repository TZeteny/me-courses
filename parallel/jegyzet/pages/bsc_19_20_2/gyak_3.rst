3. gyakorlat
============

A ``FORK`` és ``JOIN`` utasítások
---------------------------------

* Vizsgáljuk meg a ``FORK`` utasítást közvetlen megadott kódblokk és procedúra esetében!
* Adjunk példát olyan esetekre, amikor a szülő és amikor a gyermek process futása ér véget hamarabb!
* Ábrázoljuk Gannt diagramon a ``FORK`` és ``JOIN`` utasítás működését!
* Készítsünk programokat olyan esetekre, amikor a szülő várja be a gyerek processzt és fordítva!
* Oldjuk meg, hogy a szülő és a gyerek processz is ugyanahhoz a változóhoz férjen hozzá! Készítsünk példát a *lost-update* jelenség szemléltetésére!
* Írjuk át a gyökszámításos programot úgy, hogy ``FORALL`` helyett ``FORK`` és ``JOIN`` műveletekkel legyen párhuzamosítva!
* Implementáljunk egy "Oszd meg és uralkodj!" elven működő rekurzív algoritmust ``FORK``és ``JOIN`` segítségével!


Amdahl törvénye
---------------

* Vezessük le, hogy a speed-up legfeljebb a szekvenciális részek arányának reciproka lehet!

