Párhuzamos algoritmusok
=======================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   mil_1
   mil_2
   mil_3
   mil_4
   mil_5
   mpi


Feladat ötletek
---------------

* Shell rendezés
* K-Means algoritmus
* KNN algoritmus
* Medián szűrő képekre
* Dijkstra algoritmus
* Floyd-Warshall algoritmus
* Szöveg keresése fájlban
* Csúszóablakkal leíró statisztikák számítása
* Lokális küszöbölés képekre
* Gauss-Jordan elimináció
* Game of Life szimuláció
* Fraktál számítás (pl.: Mandelbrot halmaz)
* SUDOKU megoldó algoritmus
* Döntési fa építő algoritmus
