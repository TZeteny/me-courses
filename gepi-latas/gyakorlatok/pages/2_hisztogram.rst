2. Hisztogram
=============

* https://docs.opencv.org/master/d1/db7/tutorial_py_histogram_begins.html
* https://medium.com/@rndayala/image-histograms-in-opencv-40ee5969a3b7

.. todo::

   Készítsünk egy programot a hisztogram kézzel történő számításához!


Hisztogram műveletek
--------------------

* Hisztogram kiegyenlítés: https://en.wikipedia.org/wiki/Histogram_equalization

.. todo::

   Osszuk fel 4 részre a hisztogramot úgy, hogy minden részintervallumban megegyezzenek a gyakoriságok! Jelenítsük ezt meg egy szürkeárnyalatos képen, úgy hogy az adott indexű intervallumba eső képpontok a 0, 85, 170, 255 intenzitásokkal legyenek megjelenítve!

