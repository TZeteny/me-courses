3. Küszöbölés
=============

* https://docs.opencv.org/master/d7/d4d/tutorial_py_thresholding.html

Globális küszöb
---------------

.. todo::

   Készítsünk adaptív küszöbölő algoritmust, amelyik az átlag, medián és leggyakoribb elem alapján számítja a küszöbértéket!


Lokális küszöb
--------------

.. todo::

   Készítsünk egy programot, amelyik egy csúszóablak átlaga szerint küszöböl egy képet!


.. todo::

   Vizsgáljuk meg, hogy milyen eredményt ad HSV színtérben egy adott színtől való távolságra való küszöbölés!

