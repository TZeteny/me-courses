10. Rendező algoritmusok II.
============================

Gyorsrendezés
-------------

Rendezzük a következő sorozatot!

.. math::

    A = [40, 21, 15, 8, 10, 36]

* Mennyi rekurzív hívás történt a rendezés során?
* Mennyi a rekurzív hívási fa magassága?
* Mennyi csere volt szükséges a rendezéshez?
* Mennyi volt a felosztáshoz használt :math:`q` értékek minimuma és maximuma?

Négyzetes rendezés
------------------

"*Minimumkiválasztásos rendezés 2 lépésben*"

Vizsgáljuk meg a rendezést egy 16 elemű mintára!

Leszámláló rendezés
-------------------

Rendezzük az alábbi tömböt!

.. math::

   A = [3, 1, 6, 4, 3, 4, 4, 2]

* Írjuk fel a segédtömb értékét is minden lépésben!

Számjegyes rendezés
-------------------

Rendezzünk példaként 8 darab 3 jegyű számot!

Edényrendezés
-------------

Adjunk példát edényrendezésre, ahol :math:`n = 4`.

Huffman kódolás
---------------

Huffman kódolással kódoljuk a "``MA UTAT MUTATTAM``" szöveg betűit!

* Ábrázoljuk kódfát!
* Mennyi a kódfa magassága?
* Hogy néz ki a kódolt üzenet!
* Mennyi az átlagos kódhossz?
* Mekkora a tömörítési arány egy optimális, bites formában adott fix hosszúságú kódolással készített kódhoz képest?

