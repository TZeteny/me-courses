Programtervezési ismeretek
==========================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   pages/01_introduction
   pages/02_numeric_types
   pages/03_logic
   pages/04_structures
   pages/05_pseudo
   pages/06_algorithms
   pages/07_structured
   pages/08_execution
   pages/09_matrices_and_trees
   pages/10_procedures
   pages/11_convertions
   pages/12_error_handling
   pages/13_projects
   pages/14_principles


.. Köszönetnyilvánítás
.. -------------------
